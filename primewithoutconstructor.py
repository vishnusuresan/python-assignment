class Primenumber:


    def checkprimenumber(self,n):
        for i in range(2,n):
            if n%i==0:
                return False
        return True

    def __iter__(self):

            for n in range(2,50):
                if self.checkprimenumber(n):
                    yield n
def main():
    r=Primenumber()
    for n in r:
        print (n)
if __name__ == "__main__":
 main()
