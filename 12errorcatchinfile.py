try:
  buffersize=500000
  inputfile=open('newfile.txt','r')
  outfile=open('another.txt','w')
  buffer=inputfile.read(buffersize)
  while len(buffer):
      outfile.write(buffer)
      buffer=inputfile.read(buffersize)
      # once we read a particular buffersiz, pointer will reach at the end.
      print(".")
  print("done")
except IOError:
   print ("can't find data")

else:
   print ("writing successs")
